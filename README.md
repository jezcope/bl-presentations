I no longer use GitLab for this repository. You can find it at <https://codeberg.org/jezcope/bl-presentations>

The website itself is at <https://bl-presentations.erambler.co.uk>.
